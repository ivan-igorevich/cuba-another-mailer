# another mailer
## setup
`core/app.properties` should contain proper email settings. module was checked on a public SMTP server of Jino with the following setup:
```
cuba.email.fromAddress = #<user-email>
cuba.email.smtpHost = #<smtp-server>
cuba.email.smtpPort = #<smtp-port>
cuba.email.smtpStarttlsEnable=true
cuba.email.smtpAuthRequired=true
cuba.email.smtpUser = #<user-account>
cuba.email.smtpPassword = #<user-password>
```

## content
module contains the subsystem of message templates, which could be found under menu item `Administration/Notification settings` inserted after `JMX Console`.

## usage
#### templates
to create a `Mail Template` user should provide information in `String.format()` style. every dynamic attribute, inserted in the template should be marked with `%n` sequence, where `n` is the incremental integer starting with `1` (one). provided parameters in check screen should be double quoted and comma-plus-space separated. for example the template `%1, the world of %2` with parameters `"hello", "emails"` will be transformed into `hello, the world of emails`.
#### service
to use the service one should `@Inject` the `mailer_MailerService` which provides the following interface:
``` java
void sendMail(List<User> recipients, String caption, MailTemplate template, String... params) throws EmailException;
void sendMail(String role, String caption, MailTemplate template, String... params) throws EmailException;
void sendMail(List<User> recipients, String caption, String message) throws EmailException;
void sendMail(String role, String caption, String message) throws EmailException;
String createMessageFromTemplate(MailTemplate template, String... params);
MailTemplate getTemplateByName(String name)
```
usage of methods with `role` parameter is checked on another application and searches for both design-time and runtime CUBA roles (7.x support)
parameters, provided as a vararg should be `java.lang.String` which will be inserted to a template "as is", without any modifications
