package ru.iovchinnikov.mailer.web.screens.mailtemplate;

import com.haulmont.cuba.gui.Dialogs;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.app.core.inputdialog.InputParameter;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import org.slf4j.Logger;
import ru.iovchinnikov.mailer.entity.MailTemplate;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@UiController("mailer_MailTemplate.edit")
@UiDescriptor("mail-template-edit.xml")
@EditedEntityContainer("mailTemplateDc")
@LoadDataBeforeShow
public class MailTemplateEdit extends StandardEditor<MailTemplate> {
    @Inject private Notifications notifications;
    @Inject private Dialogs dialogs;
    @Inject private MessageBundle messageBundle;
    @Inject private Logger log;
    @Inject private InstanceContainer<MailTemplate> mailTemplateDc;

    /**
     * Showing a dialog to enter sample parameters of a template
     * Template parameters must be entered as
     * comma-and-space separated, double quoted values.
     * Ex.: "hello", "java", "world"
     * */
    public void checkTemplate() {
        dialogs.createInputDialog(this)
                .withCaption(messageBundle.getMessage("values"))
                .withParameter(
                        InputParameter.stringParameter("values")
                                //.withCaption(messageBundle.getMessage("values"))
                )
                .withCloseListener(inputDialogCloseEvent -> {
                    String msg = getEditedEntity().getContent();
                    String values = inputDialogCloseEvent.getValue("values");
                    assert values != null;
                    List<String> paramsList = new ArrayList<>(Arrays.asList(values.split("\", \"")));
                    for (int i = 0; i < paramsList.size(); i++) {
                        String parameter = paramsList.get(i).replace('\"', ' ');
                        msg = msg.replaceAll(("%" + (i + 1)), parameter);
                    }
                    notifications.create().withCaption(msg).show();
                })
                .show();
    }

    @Subscribe
    public void onBeforeCommitChanges(BeforeCommitChangesEvent event) {
        // Counting parameters amount
        Pattern pattern = Pattern.compile("(?<=\\%)\\d+");
        Matcher matcher = pattern.matcher(getEditedEntity().getContent());
        int count = 0;
        while (matcher.find()) {
            int val = Integer.parseInt(matcher.group());
            if (val > count) count = val;
        }
        log.info(String.format("Created or edited template with %d parameters", count));
        mailTemplateDc.getItem().setParamsAmount(count);
    }

    //TODO validate parameters (should be incremental, starting with 1 (one)
}