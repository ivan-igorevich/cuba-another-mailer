package ru.iovchinnikov.mailer.web.screens.mailtemplate;

import com.haulmont.cuba.gui.screen.*;
import ru.iovchinnikov.mailer.entity.MailTemplate;

@UiController("mailer_MailTemplate.browse")
@UiDescriptor("mail-template-browse.xml")
@LookupComponent("mailTemplatesTable")
@LoadDataBeforeShow
public class MailTemplateBrowse extends StandardLookup<MailTemplate> {
}