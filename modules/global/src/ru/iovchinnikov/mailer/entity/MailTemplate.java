package ru.iovchinnikov.mailer.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "MAILER_MAIL_TEMPLATE")
@Entity(name = "mailer_MailTemplate")
@NamePattern("%s|name")
public class MailTemplate extends StandardEntity {
    private static final long serialVersionUID = -2688744520380899590L;

    @Column(name = "NAME")
    private String name;

    @Column(name = "PARAMS_AMOUNT")
    private Integer paramsAmount;

    @Lob
    @Column(name = "CONTENT", nullable = false)
    @NotNull
    private String content;

    public Integer getParamsAmount() {
        return paramsAmount;
    }

    public void setParamsAmount(Integer paramsAmount) {
        this.paramsAmount = paramsAmount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}