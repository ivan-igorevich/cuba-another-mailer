package ru.iovchinnikov.mailer.service;

import com.haulmont.cuba.core.global.EmailException;
import com.haulmont.cuba.security.entity.User;
import ru.iovchinnikov.mailer.entity.MailTemplate;

import java.util.List;

public interface MailerService {
    String NAME = "mailer_MailerService";
    void sendMail(List<User> recipients, String caption, MailTemplate template, String... params) throws EmailException;
    void sendMail(String role, String caption, MailTemplate template, String... params) throws EmailException;
    void sendMail(List<User> recipients, String caption, String message) throws EmailException;
    void sendMail(String role, String caption, String message) throws EmailException;
    MailTemplate getTemplateByName(String name);
    String createMessageFromTemplate(MailTemplate template, String... params);
}