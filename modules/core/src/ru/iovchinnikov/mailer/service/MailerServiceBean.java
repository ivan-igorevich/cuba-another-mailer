package ru.iovchinnikov.mailer.service;

import com.haulmont.cuba.core.app.EmailerAPI;
import com.haulmont.cuba.core.global.*;
import com.haulmont.cuba.security.entity.User;
import org.springframework.stereotype.Service;
import ru.iovchinnikov.mailer.entity.MailTemplate;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Service(MailerService.NAME)
public class MailerServiceBean implements MailerService {

    @Inject private EmailerAPI emailerAPI;
    @Inject private DataManager dataManager;

    @Override
    public void sendMail(List<User> recipients, String caption, MailTemplate template, String... params) throws EmailException {
        sendMail(recipients, caption, createMessageFromTemplate(template, params));
    }

    @Override
    public void sendMail(String role, String caption, MailTemplate template, String... params) throws EmailException {
        List<User> recipients = getAllUsersByRoles(role);
        sendMail(recipients, caption, createMessageFromTemplate(template, params));
    }

    @Override
    public void sendMail(List<User> recipients, String caption, String message) throws EmailException {
        StringBuilder rx = new StringBuilder();
        recipients.forEach(user -> rx.append(user.getEmail()).append(";"));
        EmailInfo msg = EmailInfoBuilder.create()
                .setAddresses(rx.toString())
                .setBodyContentType("text/html; charset=utf-8")
                .setBody(message)
                .setCaption(caption)
                .build();
        emailerAPI.sendEmail(msg);
    }

    @Override
    public void sendMail(String role, String caption, String message) throws EmailException {
        sendMail(getAllUsersByRoles(role), caption, message);
    }

    @Override
    public String createMessageFromTemplate(MailTemplate template, String... params) {
        String msg = template.getContent();
        for (int i = 0; i < params.length; i++) {
            msg = msg.replaceAll(("%" + (i + 1)), params[i]);
        }
        return msg;
    }

    @Override
    public MailTemplate getTemplateByName(String name) {
        return dataManager.load(MailTemplate.class)
                .query("select e from mailer_MailTemplate e where e.name = :param")
                .parameter("param", name)
                .one();
    }

    private List<User> getAllUsersByRoles(String role) {
        List<User> recipients = new ArrayList<>(findUsersByRuntimeRole(role));
        recipients.addAll(findUsersByDesignRole(role));
        return recipients;
    }

    /**
     * Search users by role (old way)
     */
    private List<User> findUsersByRuntimeRole(String secRoleName) {
        return dataManager.load(User.class)
                .query("select u from sec$User u join u.userRoles ur join ur.role r where r.name = :secRoleName")
                .parameter("secRoleName", secRoleName)
                .list();
    }

    /**
     * Search users by role (7.2.x design-time roles)
     */
    private List<User> findUsersByDesignRole(String roleName) {
        return dataManager.load(User.class)
                .query("select ur.user from sec$UserRoles ur where ur.roleName = :roleName")
                .parameter("roleName", roleName)
                .list();
    }
}