-- begin MAILER_MAIL_TEMPLATE
create table MAILER_MAIL_TEMPLATE (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255),
    PARAMS_AMOUNT integer,
    CONTENT text not null,
    --
    primary key (ID)
)^
-- end MAILER_MAIL_TEMPLATE
